#!/usr/bin/env dash

cd $(dirname "$0") || exit

lastchange_url='https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2FLAST_CHANGE?alt=media'

latest_revision=$(curl -s -S "$lastchange_url")

if [ -d "$latest_revision" ]; then
	echo "Already have latest revision: $latest_revision"
	exit
else
	echo "Updating to latest revision: $latest_revision"
fi

zip_url="https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2F$latest_revision%2Fchrome-linux.zip?alt=media"

zip_file="${latest_revision}-chrome-linux.zip"

echo "fetching $zip_file from $zip_url"

mkdir "$latest_revision" || exit
(
cd "$latest_revision" || exit
aria2c -o "$zip_file" "$zip_url"

echo "unzipping $zip_file"
unzip "$zip_file"
)
if [ -d "$latest_revision" ]; then
	rm -f ./latest
	ln -s "$latest_revision/chrome-linux/" ./latest
	echo "Successfully updated to $latest_revision"
else
	echo "Failed to update to $latest_revision"
fi
